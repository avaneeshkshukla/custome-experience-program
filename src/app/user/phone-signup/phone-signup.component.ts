import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/shared/user.service";
import { NgForm } from "@angular/forms";
import { User } from "src/app/shared/user.model";
import { Router } from "@angular/router";


@Component({
  selector: "app-phone-signup",
  templateUrl: "./phone-signup.component.html",
  styleUrls: ["./phone-signup.component.css"],
})
export class PhoneSignupComponent implements OnInit {
  title = "FormValidation";
  mobileRegex = /^(\+\d{1,3}[- ]?)?\d{10}$/;
  showSucessMessage: boolean;
  serverErrorMessages: string;
  submitted = false;
  constructor(private userService: UserService, private router : Router) { }

  // onFormSubmit(form: NgForm) {
  //   this.submitted = true;
  // }

  generateOTP() {
    var digits = "0123456789abcdefghijklmnopqrstuvwxyz";
    var otpLength = 6;
    var otp = "";
    for (let i = 1; i <= otpLength; i++) {
      var index = Math.floor(Math.random() * digits.length);
      otp = otp + digits[index];
    }
    return otp;
  }

  onSubmit(form: NgForm) {
    this.submitted = true;
    this.userService.postUser(form.value).subscribe(
      res => {
        this.showSucessMessage = true;
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/userprofile');
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form);
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    );
  }

  resetForm(form: NgForm) {
    this.userService.selectedUser = {
      mobile: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }
  ngOnInit() {
    if(this.userService.isLoggedIn())
    this.router.navigateByUrl('/userprofile');
  }
}


